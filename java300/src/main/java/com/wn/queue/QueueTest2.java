package com.wn.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueTest2 {

    public static void main(String[] args) {
        //add和remove方法在失败时会抛出异常（不推荐）
        Queue<String> queue = new LinkedList<>();
        //添加元素
        queue.offer("a");
        queue.offer("b");
        for (String e : queue) {
            System.out.println("element = [" + e + "]");
        }

        System.out.println("====");
        System.out.println("poll返回第一个元素，并在队列中删除：" + queue.poll());//返回第一个元素，并在队列中删除
        for (String e : queue) {
            System.out.println("element = [" + e + "]");
        }

        System.out.println("====");
        System.out.println("element返回第一个元素：" + queue.element());//返回第一个元素
        for (String e : queue) {
            System.out.println("element = [" + e + "]");
        }

        System.out.println("====");
        System.out.println("peek返回第一个元素：" + queue.peek());//返回第一个元素
        for (String e : queue) {
            System.out.println("element = [" + e + "]");
        }
    }
}
