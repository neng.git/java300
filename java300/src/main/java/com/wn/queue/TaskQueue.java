package com.wn.queue;

import com.sun.rmi.rmid.ExecPermission;

import java.util.concurrent.*;

/**
 * 队列测试
 */
public class TaskQueue {

    //队列大下
    private final int QUEUE_LENGTH = 10000 * 10;
    //基于内存的阻塞队列
    private BlockingQueue<String> queue = new LinkedBlockingDeque<>(QUEUE_LENGTH);
    //创建计划任务执行器
    private ScheduledExecutorService es = Executors.newScheduledThreadPool(1);

    /**
     * 构造函数，执行execute方法
     */
    public TaskQueue() {
        execute();
    }

    //添加消息至队列中
    public void addQueue(String content) {
        queue.add(content);
    }

    /**
     * 初始化执行
     */
    public void execute() {
        //每一分钟执行一次
        es.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                String content = null;
                try {
                    content = queue.take();
                    System.out.println(content);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //处理队列中的信息
            }
        }, 0, 1, TimeUnit.MINUTES);
    }
}
