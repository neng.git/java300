package com.wn.gof23.celue;

/**
 * 策略模式测试
 */
public class StrategyTest {

    public static void main(String[] args) {
        int plusResult = new Plus().calculate("3+9");
        System.out.println("plusResult = [" + plusResult + "]");
    }
}