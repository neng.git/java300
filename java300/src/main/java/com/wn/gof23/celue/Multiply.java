package com.wn.gof23.celue;

/**
 * 相乘
 */
public class Multiply extends AbstractCalculator implements ICalculator {
    @Override
    public int calculate(String exp) {

        String[] ext = exp.split("\\*");
        int[] arr = new int[2];
        arr[0] = Integer.parseInt(ext[0]);

        return arr[0] * arr[1];
    }
}
