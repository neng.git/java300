package com.wn.gof23.celue;

/**
 * 统一接口
 */
public interface ICalculator {

    public int calculate(String exp);
}
