package com.wn.gof23.guanchazhe;

public class MySubject extends AbstractSubject {
    
    @Override
    public void opration() {
        System.out.println("update self");
        notifyObservers();
    }
}