package com.wn.gof23.guanchazhe;

public interface Subject {

    //增加观察者
    public void add(Observer o);

    //删除观察者
    public void delete(Observer o);

    //通知所有的观察者
    public void notifyObservers();

    //自身的操作
    public void opration();
}
