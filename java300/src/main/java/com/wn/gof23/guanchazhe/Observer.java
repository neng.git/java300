package com.wn.gof23.guanchazhe;

public interface Observer {

    public void update();
}
