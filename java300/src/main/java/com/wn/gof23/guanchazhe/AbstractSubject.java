package com.wn.gof23.guanchazhe;

import java.util.Enumeration;
import java.util.Vector;

public abstract class AbstractSubject implements Subject {

    private Vector<Observer> vector = new Vector<Observer>();

    @Override
    public void add(Observer o) {
        vector.add(o);
    }

    @Override
    public void delete(Observer o) {
        vector.remove(o);
    }

    @Override
    public void notifyObservers() {
        Enumeration<Observer> enums = vector.elements();
        while (enums.hasMoreElements()) {
            enums.nextElement().update();
        }
    }
}
