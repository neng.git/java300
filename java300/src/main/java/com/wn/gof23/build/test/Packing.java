package com.wn.gof23.build.test;

/**
 * 食物包装接口
 */
public interface Packing {

    public String pack();
}
