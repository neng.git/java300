package com.wn.gof23.moban;

/**
 * 西红柿炒蛋
 */
public class EggsWithTomato extends DodishTemplate {


    @Override
    public void preparation() {
        System.out.println("准备食材。。。");
    }

    @Override
    public void doing() {
        System.out.println("做菜。。。。");
    }

    @Override
    public void carriedDishes() {
        System.out.println("上菜。。。。");
    }
}
