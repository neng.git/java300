package com.wn.gof23.moban;

/**
 * 举例说明：
 * 做菜可以分为3个步骤：
 * 1、备料
 * 2、具体做菜
 * 3、上菜
 * 这三步是骨架，然而做不同菜需要的料，做的方法，以及如何上菜就是不同的实现细节
 */
public abstract class DodishTemplate {

    /**
     * 具体的整个过程
     */
    protected void doDish() {
        this.preparation();
        this.doing();
        this.carriedDishes();
    }

    /**
     * 1、备料
     */
    public abstract void preparation();

    /**
     * 2、做菜
     */
    public abstract void doing();

    /**
     * 3、上菜
     */
    public abstract void carriedDishes();

}
