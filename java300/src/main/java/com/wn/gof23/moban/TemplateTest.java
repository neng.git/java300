package com.wn.gof23.moban;

/**
 * 模板模式测试
 */
public class TemplateTest {

    public static void main(String[] args) {

        DodishTemplate eggWithTomato = new EggsWithTomato();
        eggWithTomato.doDish();
    }

}
